from django.urls import path
from category.restful.views import questions_view, categories_view, category_activities_view, category_score_view

urlpatterns = [
    path('questions/', questions_view),
    path('categories/', categories_view),
    path('<int:category_id>/activities/', category_activities_view),
    path('<int:category_id>/scores/', category_score_view),
]