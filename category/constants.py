from model_utils import Choices


ACTIVITY_ANSWER_TYPE_CHOICES = Choices(
    (0, 'duration', 'Duration'),
    (1, 'int', 'Integer'),
)