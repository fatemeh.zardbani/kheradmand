from django.db import models

from user.models import Profile
from utils.models import BaseModel
from .constants import ACTIVITY_ANSWER_TYPE_CHOICES


# TODO: add category to the first of every model name


class Category(BaseModel):
    name = models.CharField(max_length=50, unique=True)
    priority_weight = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('-priority_weight', 'id')

    def __str__(self):
        return self.name


class Question(BaseModel):
    category = models.ForeignKey(Category, related_name='questions', on_delete=models.PROTECT)
    question = models.TextField()
    priority_weight = models.PositiveIntegerField(default=0)
    required_answer = models.ForeignKey('PossibleAnswer', related_name='follow_up_questions', on_delete=models.PROTECT,
                                        blank=True, null=True)

    class Meta:
        ordering = ('-category__priority_weight', '-priority_weight', 'id')

    def __str__(self):
        return self.question


class PossibleAnswer(BaseModel):
    question = models.ForeignKey('Question', related_name='possible_answers', on_delete=models.PROTECT)
    answer = models.TextField()
    point = models.IntegerField()
    priority_weight = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ('-priority_weight', 'id')

    def __str__(self):
        return f'{self.question} - {self.answer}'


class CategoryActivity(BaseModel):
    category = models.ForeignKey(Category, related_name='category_activities', on_delete=models.PROTECT)
    description_text = models.TextField()
    description_picture = models.ImageField(upload_to='activity_pictures/')
    name = models.TextField()
    score_to_add = models.IntegerField()
    activity_image_mongo_id = models.TextField(null=True, blank=True)


    def __str__(self):
        return str(self.category) + str(self.name)


class CategoryActivityQuestion(BaseModel):
    activity = models.ForeignKey(CategoryActivity, related_name='questions', on_delete=models.PROTECT)
    title = models.TextField()
    answer_type = models.IntegerField(choices=ACTIVITY_ANSWER_TYPE_CHOICES)

    def __str__(self):
        return str(self.activity) + str(self.title) + str(self.answer_type)


class CategoryScore(BaseModel):
    profile = models.ForeignKey(Profile, related_name='scores', on_delete=models.PROTECT)
    category = models.ForeignKey(Category, related_name='user_scores', on_delete=models.PROTECT)
    score = models.IntegerField()

    def __str__(self):
        return str(self.profile) + str(self.category) + str(self.score)
