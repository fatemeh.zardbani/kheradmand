from django.db.models import Sum
from rest_framework import serializers

from category.models import Category, CategoryActivity, CategoryActivityQuestion, CategoryScore, PossibleAnswer, \
    Question


class CategorySerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'score')
        read_only_fields = ('id', 'name', 'score')

    def get_score(self, obj):
        return CategoryScore.objects.filter(profile__user=self.context['request'].user, category=obj).aggregate(
            Sum('score'))


class CategoryActivityQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryActivityQuestion
        fields = ('id', 'title', 'answer_type')


class CategoryActivitySerializer(serializers.ModelSerializer):
    questions = CategoryActivityQuestionSerializer(read_only=True, many=True)
    category_id = serializers.ReadOnlyField(source='category.id')

    class Meta:
        model = CategoryActivity
        fields = ('id', 'name','activity_image_mongo_id', 'description_text', 'description_picture', 'score_to_add', 'questions', 'category_id')


class PossibleAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PossibleAnswer
        fields = ('id', 'answer')
        read_only_fields = ('id', 'answer')


class QuestionSerializer(serializers.ModelSerializer):
    category = serializers.ReadOnlyField(source='category.name')
    possible_answers = PossibleAnswerSerializer(read_only=True, many=True)

    class Meta:
        model = Question
        fields = ('category', 'question', 'required_answer', 'possible_answers')
        read_only_fields = ('category', 'question', 'required_answer', 'possible_answers')
