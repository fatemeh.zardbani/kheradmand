from rest_framework import viewsets
from category.restful.serializers import QuestionSerializer, CategorySerializer, CategoryActivitySerializer, CategoryScore
from category.models import Question, Category, CategoryActivity
from rest_framework.views import APIView
from rest_framework.response import Response


class CategoryView(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects

class QuestionView(viewsets.ModelViewSet):
    serializer_class = QuestionSerializer
    queryset = Question.objects

class CategoryActivityView(viewsets.ModelViewSet):
    serializer_class = CategoryActivitySerializer
    
    def get_queryset(self):
        category_id = self.kwargs.get('category_id')
        return CategoryActivity.objects.filter(category_id=category_id)

class CategoryScoreView(APIView):
    def get(self, request, *args, **kwargs):
        result = []
        summ = 0
        user_scores = CategoryScore.objects.filter(profile=request.user.profile, category_id=kwargs['category_id'])
        for us in user_scores:
            summ += us.score
            result += [{'date': us.created, 'score':summ}]
        return Response(result)

questions_view = QuestionView.as_view({'get': 'list'})
categories_view = CategoryView.as_view({'get': 'list'})
category_activities_view = CategoryActivityView.as_view({'get': 'list'})
category_score_view = CategoryScoreView.as_view()

