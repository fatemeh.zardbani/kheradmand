from django.contrib import admin
from .models import Category, Question, PossibleAnswer, CategoryActivity, CategoryActivityQuestion, CategoryScore


admin.site.register(Category)
admin.site.register(Question)
admin.site.register(PossibleAnswer)
admin.site.register(CategoryActivity)
admin.site.register(CategoryActivityQuestion)
admin.site.register(CategoryScore)
