from rest_framework import serializers
from activity.models import Activity, ActivityAnswer
from category.models import CategoryActivity, CategoryActivityQuestion


class ActivityAnswerSerializer(serializers.ModelSerializer):
    question = serializers.ReadOnlyField(source='question.title')
    answer_type = serializers.ReadOnlyField(source='question.answer_type')

    class Meta:
        model = ActivityAnswer
        fields = ('id', 'answer_text', 'question', 'answer_type')

class QASerializer(serializers.Serializer):
    question = serializers.IntegerField()
    answer = serializers.CharField()

class ActivitySerializer(serializers.ModelSerializer):
    qa_response = serializers.ListField(child=QASerializer(), required=True, write_only=True)
    answers = ActivityAnswerSerializer(read_only=True, many=True)
    activity_name = serializers.ReadOnlyField(source='category_activity.name')
    activity_description_text = serializers.ReadOnlyField(source='category_activity.description_text')
    category_id = serializers.ReadOnlyField(source='category_activity.category.id')
    # activity_description_picture = serializers.ReadOnlyField(source='category_activity.description_picture')

    class Meta:
        model = Activity
        fields = ('id', 'qa_response', 'answers', 'category_activity', 'start_date', 'activity_name', 'activity_description_text', 'category_id', 'done')
        read_only_fields = ('answers', 'category_activity', 'activity_name', 'activity_description_text', 'category_id', 'done')
    
    def create(self, validated_data):
        qa_response = validated_data.pop('qa_response')
        activity = super().create(validated_data)
        answers = []
        for qa in qa_response:
            answers.append(ActivityAnswer(activity=activity, question_id=qa.get('question'), answer_text=qa.get('answer')))
        # TODO: validator for survey (check all questions)
        ActivityAnswer.objects.bulk_create(answers)
        return activity
    
    def update(self, instance, validated_data):
        instance.done = validated_data.get('done', instance.done)
        instance.save()
        return instance

