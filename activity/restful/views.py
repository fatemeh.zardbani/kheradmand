from rest_framework import viewsets
from rest_framework.response import Response

from activity.models import Activity
from activity.restful.serializers import ActivitySerializer
from category.models import CategoryScore


class ActivityView(viewsets.ModelViewSet):
    serializer_class = ActivitySerializer

    def perform_create(self, serializer):
        serializer.save(profile=self.request.user.profile, category_activity_id=self.kwargs.get('category_activity_id'))

    def get_queryset(self):
        category_id = self.kwargs.get('category_id')
        if category_id is None:
            return Activity.objects.filter(profile__user=self.request.user)
        return Activity.objects.filter(profile__user=self.request.user, category_activity__category_id=category_id)

    def get_object(self):
        return self.get_queryset().first()

    def partial_update(self, request, *args, **kwargs):
        instance = Activity.objects.get(pk=self.kwargs['activity_id'])
        if instance.done is False:
            instance.done = True
            instance.save()
            CategoryScore.objects.create(profile=instance.profile, category=instance.category_activity.category,
                                         score=instance.category_activity.score_to_add)
        serializer = self.serializer_class(instance)
        return Response(serializer.data)


create_activity_view = ActivityView.as_view({'post': 'create'})
update_activity_view = ActivityView.as_view({'put': 'partial_update'})
activity_list_view = ActivityView.as_view({'get': 'list'})
latest_activity_view = ActivityView.as_view({'get': 'retrieve'})
category_activity_view = ActivityView.as_view({'get': 'list'})
