from utils.models import BaseModel
from django.db import models
from category.models import CategoryActivity, CategoryActivityQuestion
from user.models import Profile
from category.constants import ACTIVITY_ANSWER_TYPE_CHOICES


class Activity(BaseModel):
    profile = models.ForeignKey(Profile, related_name='activities', on_delete=models.PROTECT)
    category_activity = models.ForeignKey(CategoryActivity, related_name='activities', on_delete=models.PROTECT)
    
    # TODO: check if all questions have duration
    start_date = models.DateField()
    done = models.BooleanField(default=False)

    def __str__(self):
        return str(self.profile) + str(self.category_activity) + str(self.done)

class ActivityAnswer(BaseModel):
    activity = models.ForeignKey(Activity, related_name='answers', on_delete=models.PROTECT)
    question = models.ForeignKey(CategoryActivityQuestion, related_name='user_answers', on_delete=models.PROTECT)

    answer_text = models.TextField()

    @property
    def answer(self):
        answer_type = self.question.answer_type
        if answer_type == ACTIVITY_ANSWER_TYPE_CHOICES.duration:
            # TODO : fix this
            return self.answer_text
        elif answer_type == ACTIVITY_ANSWER_TYPE_CHOICES.int:
            return int(self.answer_text)
        else:
            return self.answer_text
    
    def __str__(self):
        return str(self.activity) + str(self.question) + str(self.answer_text)
