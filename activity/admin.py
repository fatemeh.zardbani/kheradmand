from django.contrib import admin
from activity.models import Activity, ActivityAnswer


admin.site.register(Activity)
admin.site.register(ActivityAnswer)