from django.urls import path
from activity.restful.views import create_activity_view, activity_list_view, latest_activity_view, category_activity_view, update_activity_view

urlpatterns = [
    path('<int:category_activity_id>/submit/', create_activity_view),
    path('list/', activity_list_view),
    path('category/<int:category_id>/list/', category_activity_view),
    path('latest/', latest_activity_view),
    path('update/<int:activity_id>/', update_activity_view),
]