from __future__ import print_function

from __future__ import absolute_import, unicode_literals
import random
from celery.decorators import task
import datetime
from activity.restful.views import ActivityView
from activity.models import Activity
from activity.restful.serializers import ActivitySerializer
from category.models import CategoryActivity
from fcm_tokens.models import FcmToken
from fcm_tokens.restful.serializers import FcmTokenSerializer
import firebase_admin
from firebase_admin import credentials
from firebase_admin import messaging

cred = credentials.Certificate("kheradmand-c3b09-firebase-adminsdk-cv0f9-2a03d5a75f.json")
firebase_admin.initialize_app(cred)

@task(name="get_activity_list")
def get_activity_list():
    print('GET A:'+str(len(Activity.objects.all())))
    activity_list = Activity.objects.all()
    print('7week'+str(datetime.datetime.today() - datetime.timedelta(days=7)))
    previous_week_time=datetime.datetime.today() - datetime.timedelta(days=7)
    previous_week_date=datetime.date(previous_week_time.year,previous_week_time.month,previous_week_time.day)
    print('DATE'+str(datetime.date.today().day+1))
    # activity_list = Activity.objects.filter(done=False).filter(category_activity__name='پیاده روی').filter(
    #     start_date__lte=datetime.date.today())
    activity_list = Activity.objects.filter(done=False).filter(
        start_date__lte=datetime.date.today())
    for activity in activity_list:
        check_and_send_notification(activity)

    print('activityLIST:'+str((activity_list.count())))
    activity_serializer = ActivitySerializer(activity_list,many=True)
    print('********')
    return activity_serializer.data


def check_and_send_notification(activity):
    print('IDDDDDDDD:'+str(activity.profile))
    activity_serializer=ActivitySerializer(activity)
    answers=activity_serializer.data['answers']
    number_of_week=0
    per_week=1
    last_notification=False
    for answer in answers:
        if answer['question']=='چندهفته' or answer['question'] == 'برای چندهفته':
            number_of_week=int(answer['answer_text'])
        if answer['question']=='چندباردر هفته' or answer['question'] == 'چندبار در هفته':
            per_week=int(answer['answer_text'])
            if per_week>7:
                per_week=1
    if number_of_week ==0 or per_week ==0:
        return

    date_time_start=datetime.datetime.strptime(activity_serializer.data['start_date'], '%Y-%m-%d')
    date_time_today=datetime.datetime.today()
    # - datetime.timedelta(days=7)
    if (date_time_start+datetime.timedelta(days=7*number_of_week))< date_time_today:
        return


    week_index=int(float((date_time_today-date_time_start).total_seconds()/(7*24*60*60)))
    day_step=int(float(7/per_week))
    day_index=0

    while day_index<7:
        if((date_time_start+datetime.timedelta(days=(7*week_index+day_index))).day == date_time_today.day):
            if week_index + 1 == number_of_week and day_index+1==per_week:
                last_notification=True
            send_notif(activity,last_notification)
        day_index+=1


    print('weekIndex'+str(week_index))
    # print('********'+str(activity_serializer.data['profile']))
    print(str(date_time_today))

    print(str(date_time_start))
    print(str((date_time_today-date_time_start).total_seconds()))
    print(str(int(number_of_week)+100000))

def send_notif(activity,last_notification):
    from firebase_admin import messaging
    activity_serializer=ActivitySerializer(activity)
    fcm_token_qs=FcmToken.objects.filter(profile=activity.profile)
    fcm_tokens_serializer=FcmTokenSerializer(fcm_token_qs,many=True)
    fcm_token_list=[]
    for fcm_token in fcm_tokens_serializer.data:
        fcm_token_list.append(fcm_token['token_value'])
    # Create a list containing up to 100 registration tokens.
    # These registration tokens come from the client FCM SDKs.
    registration_tokens = fcm_token_list
    if len(registration_tokens) > 0:
        message = messaging.MulticastMessage(
            notification=messaging.Notification(
                title='همیاد',
                body=str(activity_serializer.data['activity_name'])
            ),
            data={'activity_id': str(activity_serializer.data['id']), 'last_notification': str(last_notification)},
            tokens=registration_tokens,
        )
        response = messaging.send_multicast(message)
        # Response is a message ID string.
        print('Successfully sent message:', response)


import schedule
import time
schedule.every().day.at("09:00").do(get_activity_list)
from threading import Thread

def start_run_check_notif():
    while True:
        schedule.run_pending()
        time.sleep(1)

def run_notif_thread():
    thread = Thread(target=start_run_check_notif, args=())
    thread.start()
    # thread.join()
    print('thread started')