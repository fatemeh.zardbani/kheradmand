import random

from rest_framework import serializers

from category.models import Category, CategoryScore
from game.models import GameLevel, PlayedMatch, Word


class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = ('word_text', 'hint')


class GameLevelSerializer(serializers.ModelSerializer):
    words = serializers.SerializerMethodField()

    class Meta:
        model = GameLevel
        fields = ('score_to_add', 'words')

    def get_words(self, obj):
        # TODO: Change the sampling process
        this_level_words = Word.objects.filter(game_level=obj)
        sample = random.sample(list(this_level_words), k=obj.word_count)
        return WordSerializer(sample, many=True).data


class PlayedMatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlayedMatch
        fields = ('score',)

    def create(self, validated_data):
        # TODO: persian name
        mental_category = Category.objects.filter(name='سلامت روان').first()
        CategoryScore.objects.create(profile=validated_data['profile'], category=mental_category,
                                     score=validated_data['score'])
        return super().create(validated_data)
