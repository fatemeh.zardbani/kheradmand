from rest_framework import viewsets

from game.models import GameLevel, PlayedMatch
from game.restful.serializers import GameLevelSerializer, PlayedMatchSerializer


class GameLevelRetrieveView(viewsets.ModelViewSet):
    serializer_class = GameLevelSerializer

    def get_object(self):
        level = self.kwargs.get('level')
        game_level = GameLevel.objects.filter(level_name=level)
        return game_level.first()


class PlayedMatchCreateView(viewsets.ModelViewSet):
    serializer_class = PlayedMatchSerializer
    queryset = PlayedMatch.objects

    def perform_create(self, serializer):
        serializer.save(profile=self.request.user.profile)


game_level_retrieve_view = GameLevelRetrieveView.as_view({'get': 'retrieve'})
played_match_create_view = PlayedMatchCreateView.as_view({'post': 'create'})
