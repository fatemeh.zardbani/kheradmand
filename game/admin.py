from django.contrib import admin
from .models import GameLevel, Word, PlayedMatch

admin.site.register(GameLevel)
admin.site.register(Word)
admin.site.register(PlayedMatch)
