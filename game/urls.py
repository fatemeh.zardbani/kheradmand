from django.urls import path
from game.restful.views import game_level_retrieve_view, played_match_create_view

urlpatterns = [
    path('play/<int:level>/', game_level_retrieve_view),
    path('score/', played_match_create_view),   
]