from utils.models import BaseModel
from django.db import models
from user.models import Profile
import datetime
from .constants import GAME_LEVEL_CHOICES, GAME_LEVEL_WORD_COUNT


# TODO: implement the __str__ method for these models

class GameLevel(BaseModel):
    level_name = models.IntegerField(choices=GAME_LEVEL_CHOICES, unique=True)
    score_to_add = models.IntegerField(default=0)
    word_count = models.IntegerField(default=GAME_LEVEL_WORD_COUNT)

    def __str__(self):
        return str(self.level_name)

class Word(BaseModel):
    word_text = models.CharField(max_length=40, db_index=True)
    hint = models.CharField(max_length=100)
    game_level = models.ForeignKey(GameLevel, on_delete=models.CASCADE)

    def __str__(self):
        return self.word_text

class PlayedMatch(BaseModel):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)

    def __str__(self):
        return str(self.profile) + str(self.score)



