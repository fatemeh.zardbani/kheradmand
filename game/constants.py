from model_utils import Choices

GAME_LEVEL_CHOICES =  Choices(
    (0, 'easy', 'ساده'), 
    (1, 'medium', 'متوسط'), 
    (2, 'hard', 'سخت'), 
    (3, 'evil', 'شیطانی')
)
GAME_LEVEL_WORD_COUNT = 10
