from django.db import models
from django.db.models import Count, F, Q

from category.models import PossibleAnswer
from user.models import Profile
from utils.models import BaseModel


class Survey(BaseModel):
    profile = models.ForeignKey(Profile, related_name='surveys', on_delete=models.PROTECT)

    class Meta:
        ordering = ('-created',)

    def update_results(self):
        results = PossibleResult.objects.annotate(
            answers_count=Count('answers', distinct=True),
            survey_answers_count=Count('answers', filter=Q(answers__survey_answers__survey_id=self.id), distinct=True)
        ).filter(answers_count=F('survey_answers_count'))
        self.results.clear()
        self.results.add(*results)

    def __str__(self):
        return str(self.created) + str(self.profile)


class SurveyAnswer(BaseModel):
    survey = models.ForeignKey('Survey', related_name='answers', on_delete=models.PROTECT)
    answer = models.ForeignKey('category.PossibleAnswer', related_name='survey_answers', on_delete=models.PROTECT)

    def __str__(self):
        return str(self.survey) + str(self.answer)


class PossibleResult(BaseModel):
    answers = models.ManyToManyField('category.PossibleAnswer', related_name='possible_results')
    surveys = models.ManyToManyField('Survey', related_name='results')

    result = models.TextField()

    @property
    def category_id(self):
        return self.answers.first().question.category_id

    def __str__(self):
        return self.result[:20]
