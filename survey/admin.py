from django.contrib import admin

from survey.models import PossibleResult
from .models import Survey, SurveyAnswer


class PossibleResultAdmin(admin.ModelAdmin):
    fields = ('answers', 'result')
    filter_horizontal = ('answers',)


admin.site.register(Survey)
admin.site.register(SurveyAnswer)
admin.site.register(PossibleResult, PossibleResultAdmin)
