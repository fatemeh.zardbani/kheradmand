from rest_framework import serializers

from category.models import Category, CategoryScore, PossibleAnswer
from survey.models import PossibleResult, Survey, SurveyAnswer


class SurveyAnswerSerializer(serializers.ModelSerializer):
    # TODO: refactor "answer" and "question" names
    question = serializers.CharField(source='answer.question.question')
    answer = serializers.CharField(source='answer.answer')

    class Meta:
        model = SurveyAnswer
        fields = ('question', 'answer')


class SurveySerializer(serializers.ModelSerializer):
    answer_ids = serializers.ListField(child=serializers.IntegerField(), required=True, write_only=True)
    answers = SurveyAnswerSerializer(read_only=True, many=True)

    class Meta:
        model = Survey
        fields = ('answer_ids', 'created', 'answers')

    def make_scores(self, answer_ids, profile):
        category_score_values = {c: 0 for c in Category.objects.all()}
        for a_id in answer_ids:
            category_score_values[
                Category.objects.filter(questions__possible_answers=a_id).first()] += PossibleAnswer.objects.filter(
                pk=a_id).first().point
        for cat in category_score_values:
            CategoryScore.objects.create(profile=profile, category=cat, score=category_score_values[cat])

    def create(self, validated_data):
        answer_ids = validated_data.pop('answer_ids')
        self.make_scores(answer_ids, validated_data['profile'])
        survey = super().create(validated_data)
        answers = []
        for answer_id in answer_ids:
            answers.append(SurveyAnswer(survey=survey, answer_id=answer_id))
        # TODO: validator for survey (check all questions)
        SurveyAnswer.objects.bulk_create(answers)
        survey.update_results()
        return survey


class ResultAnswerSerializer(serializers.ModelSerializer):
    question = serializers.SerializerMethodField()

    class Meta:
        model = PossibleAnswer
        fields = ('question', 'answer')
        read_only_fields = ('question', 'answer')

    def get_question(self, obj):
        return obj.question.question


class SurveyResultSerializer(serializers.ModelSerializer):
    answers = ResultAnswerSerializer(many=True, read_only=True)

    class Meta:
        model = PossibleResult
        fields = ('category_id', 'answers', 'result')
        read_only_fields = ('category_id', 'answers', 'result')
