from rest_framework import viewsets

from survey.models import Survey
from survey.restful.serializers import SurveyResultSerializer, SurveySerializer


class SurveyView(viewsets.ModelViewSet):
    serializer_class = SurveySerializer

    def perform_create(self, serializer):
        serializer.save(profile=self.request.user.profile)

    def get_queryset(self):
        return Survey.objects.filter(profile__user=self.request.user)

    def get_object(self):
        return self.get_queryset().first()


class SurveyResultView(viewsets.ModelViewSet):
    serializer_class = SurveyResultSerializer

    def get_queryset(self):
        latest_survey = Survey.objects.filter(profile__user=self.request.user).first()
        return latest_survey.results.all().prefetch_related('answers', 'answers__question')


create_survey_view = SurveyView.as_view({'post': 'create'})
survey_list_view = SurveyView.as_view({'get': 'list'})
latest_survey_view = SurveyView.as_view({'get': 'retrieve'})
latest_survey_results_view = SurveyResultView.as_view({'get': 'list'})
