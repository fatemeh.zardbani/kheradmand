from django.urls import path

from survey.restful.views import create_survey_view, latest_survey_results_view, latest_survey_view, survey_list_view

urlpatterns = [
    path('submit/', create_survey_view),
    path('list/', survey_list_view),
    path('latest/', latest_survey_view),
    path('results/', latest_survey_results_view)
]
