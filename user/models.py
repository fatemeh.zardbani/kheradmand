from django.contrib.auth.models import User
from django.db import models

from user.constants import ROLE_CHOICES
from utils.models import BaseModel
from .constants import SEX_CHOICES


class Profile(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    date_of_birth = models.DateField(db_index=True, null=True, blank=True)
    sex = models.IntegerField(choices=SEX_CHOICES, null=True, blank=True)
    role = models.IntegerField(choices=ROLE_CHOICES, null=True, blank=True)

    def __str__(self):
        return f'{self.user}'
