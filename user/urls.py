from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token

from social_network.restful.mongo_tool import upload_image_view
from user.restful.views import profile_view, register_view, logout_view

urlpatterns = [
    path('login/', obtain_jwt_token),
    path('profile/', profile_view),
    path('register/', register_view),
    path('logout/', logout_view),
    path('upload_image/', upload_image_view),

]


