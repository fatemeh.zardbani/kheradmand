from model_utils import Choices

SEX_CHOICES = Choices(
    (0, 'male', 'مرد'),
    (1, 'female', 'زن')
)
ROLE_CHOICES = Choices(
    (0, 'parent', 'والدین'),
    (1, 'grand_parents', 'پدر/مادر بزرگ'),
    (2, 'elder', 'سالمند'),
    (3, 'elder_child', 'فرزند'),
    (4, 'elder_grand_child', 'نوه'),
    (5, 'care_taker', 'مراقبت کننده')

)
