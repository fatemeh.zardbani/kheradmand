from django.contrib.auth.models import User
from rest_framework import serializers

from user.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')
    first_name = serializers.ReadOnlyField(source='user.first_name')

    class Meta:
        model = Profile
        fields = ('username', 'first_name', 'sex', 'role', 'date_of_birth')


class RegisterSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    first_name = serializers.CharField(source='user.first_name')
    password = serializers.CharField(source='user.password', write_only=True)

    class Meta:
        model = Profile
        fields = ('username', 'first_name', 'password', 'sex', 'date_of_birth', 'role')

    def create(self, validated_data):
        # request = self.context.get("request")
        # print(str(request.data))
        user = validated_data.pop('user')
        username = user.pop('username')
        first_name = user.pop('first_name')
        password = user.pop('password')
        user = User.objects.create_user(username=username, password=password, first_name=first_name)
        validated_data['user'] = user
        return super().create(validated_data)
