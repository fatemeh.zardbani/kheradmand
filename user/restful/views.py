from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from fcm_tokens.models import FcmToken
from user.models import Profile
from user.restful.serializers import ProfileSerializer, RegisterSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets


class ProfileView(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer
    queryset = Profile.objects

    def get_object(self):
        return self.request.user.profile


class RegisterView(viewsets.ModelViewSet):
    serializer_class = RegisterSerializer
    queryset = Profile.objects
    permission_classes = [AllowAny]


class LogOut(APIView):
    def post(self, request, format=None):
        # simply delete the token to force a login
        # request.user.auth_token.delete()
        print('LOGOUT:'+str(request.data))
        FcmToken.objects.filter(token_value=request.data['token_value']).delete()
        return Response(status=status.HTTP_200_OK)


profile_view = ProfileView.as_view({'get': 'retrieve', 'post': 'partial_update'})
register_view = RegisterView.as_view({'post': 'create'})
logout_view = LogOut.as_view()
