# Generated by Django 2.2.3 on 2019-10-07 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_auto_20190729_1245'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='role',
            field=models.IntegerField(choices=[(0, 'سالمند'), (1, 'فرزند سالمند'), (2, 'نوه سالمند'), (3, 'مراقب سالمند')], default=0),
        ),
    ]
