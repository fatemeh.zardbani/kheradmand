from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
import os
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE','Kheradmand.settings')

app = Celery('proj')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


from celery.schedules import crontab
app.conf.beat_schedule = {
    'add-every-day-contrab': {
        'task': 'get_activity_list',
        # Execute daily at midnight.
        'schedule': crontab(minute=0, hour=0)
        # 'args': (16, 16),
    },
}
@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))