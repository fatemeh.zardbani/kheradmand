from django.urls import path

from social_network.restful.mongo_tool import get_media
from social_network.restful.views import get_post_view, like_post_view, post_create_view, posts_list_view, \
    unlike_post_view

urlpatterns = [
    path('submit/', post_create_view),
    path('list/', posts_list_view),
    path('get/<int:post_id>/', get_post_view),
    path('<int:post_id>/like/', like_post_view),
    path('<int:post_id>/unlike/', unlike_post_view),
    path('mongo_media/<slug:slug>/', get_media),

]
