from django.db import models

from user.models import Profile
from utils.models import BaseModel


class Post(BaseModel):
    profile = models.ForeignKey(Profile, related_name='posts', on_delete=models.PROTECT)
    post_text = models.TextField()
    post_picture = models.ImageField(upload_to='post_pictures/', null=True, blank=True)
    post_image_mongo_id = models.TextField(null=True, blank=True)


    class Meta:
        ordering = ('-created', 'id')


class Like(BaseModel):
    post = models.ForeignKey(Post, related_name='likes', on_delete=models.PROTECT)
    liker_profile = models.ForeignKey(Profile, related_name='user_likes', on_delete=models.PROTECT)
