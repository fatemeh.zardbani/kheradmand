import base64

from django.core.files.base import ContentFile
from rest_framework import serializers

from category.models import Category, CategoryScore
from social_network.models import Like, Post
from user.restful.serializers import ProfileSerializer


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, str) and data.startswith('data:image'):
            # base64 encoded image - decode
            img_format, img_str = data.split(';base64,')  # format ~= data:image/X,
            ext = img_format.split('/')[-1]  # guess file extension
            data = ContentFile(base64.b64decode(img_str), name='temp.' + ext)
            return super().to_internal_value(data)
        return None



class PostSerializer(serializers.ModelSerializer):
    number_of_likes = serializers.SerializerMethodField()
    profile = ProfileSerializer(read_only=True, many=False)
    liked = serializers.SerializerMethodField()
    # post_picture = Base64ImageField()

    class Meta:
        model = Post
        fields = ('id', 'profile', 'post_text', 'post_image_mongo_id', 'number_of_likes', 'liked')
        read_only_fields = ('id', 'number_of_likes', 'profile', 'liked')

    def get_number_of_likes(self, obj):
        return Like.objects.filter(post=obj).count()

    def get_liked(self, obj):
        return Like.objects.filter(post=obj, liker_profile=self.context['request'].user.profile).exists()

    def create(self, validated_data):
        # TODO: persian name
        social_category = Category.objects.filter(name='سلامت اجتماعی').first()
        CategoryScore.objects.create(profile=validated_data['profile'], category=social_category, score=1)
        return super().create(validated_data)


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ()
