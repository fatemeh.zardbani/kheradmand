import base64

import gridfs
from bson import ObjectId
from django.http import HttpResponse
from pymongo import MongoClient
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework import status
from rest_framework.response import Response

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

# TODO place mongoURL, username and password or Create IMGDJANGO mongo DB locally.
client=MongoClient("mongodb+srv://mjuser:salamsalam12345@cluster0-fumtc.mongodb.net/test?retryWrites=true&w=majority")
DB=client.get_database('IMGDJANGO1')
# DB=MongoClient().IMGDJANO1
FS = gridfs.GridFS(DB)

def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def upload_file(request):
    if request.method == 'POST':
        file = request.FILES['post_picture']
        if file and allowed_file(file.name):
            # filename = secure_filename(file.filename)
            filename = file.name
            oid = FS.put(file, content_type=file.content_type, filename=filename)
            return str(oid)

def savebase64toMongo(request):
    try:
        id=''
        if len(request.data['post_picture']) > 0:
            img_format, img_str = request.data['post_picture'].split(';base64,')  # format ~= data:image/X,
            ext = img_format.split('/')[-1]  # guess file extension
            print('img_formant'+str(img_format))
            # data = ContentFile(base64.b64decode(img_str), name='temp.' + ext)
            # id = FS.put(base64.b64decode(img_str), content_type='image/jpeg', filename='testbase64.jpeg')
            id = FS.put(base64.b64decode(img_str), content_type=img_format)
        return id
    except:
        return ''
    # In Case of FormData
    # oid = FS.put(data, content_type='image/jpeg', filename='testbase64.jpeg')
    # print(str(oid))
    # return str(oid)
    # if request.method == 'POST':
    #     file = request.FILES['post_picture']
    #     if file and allowed_file(file.name):
    #         # filename = secure_filename(file.filename)
    #         filename = file.name
    #         oid = FS.put(file, content_type=file.content_type, filename=filename)
    #         print('OID:'+oid)
    #         return str(oid)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_media(request, slug):
    if slug == '' or slug is None:
        return HttpResponse('', content_type='')
    image_data = FS.get(ObjectId(slug))
    return HttpResponse(image_data, content_type =image_data.content_type)

@api_view(['POST'])
@permission_classes([AllowAny])
def upload_image_view(request):
    try:
        id= upload_file(request)
        return Response(id,status=status.HTTP_200_OK)
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)
