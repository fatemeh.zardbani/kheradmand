from rest_framework import status, viewsets
from rest_framework.response import Response

from social_network.models import Like, Post
from social_network.restful.mongo_tool import upload_file, savebase64toMongo
from social_network.restful.serializers import LikeSerializer, PostSerializer


class PostView(viewsets.ModelViewSet):
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        # print(str(serializer))
        # post_image_mongo_id = upload_file(self.request)
        post_image_mongo_id=savebase64toMongo(self.request)
        serializer.save(profile=self.request.user.profile, post_image_mongo_id=post_image_mongo_id)

    def get_queryset(self):
        return Post.objects.all()

    def get_object(self):
        return self.get_queryset().get(id=self.kwargs.get('post_id'))


class LikeView(viewsets.ModelViewSet):
    serializer_class = LikeSerializer

    def get_queryset(self):
        return Post.objects.all()

    def get_object(self):
        return self.get_queryset().get(id=self.kwargs.get('post_id'))

    def like(self, request, *args, **kwargs):
        instance, created = Like.objects.get_or_create(liker_profile=self.request.user.profile, post=self.get_object())
        return Response(status=status.HTTP_201_CREATED if created else status.HTTP_400_BAD_REQUEST)

    def unlike(self, request, *args, **kwargs):
        deleted = bool(Like.objects.filter(liker_profile=self.request.user.profile, post=self.get_object()).delete())
        return Response(status=status.HTTP_200_OK if deleted else status.HTTP_400_BAD_REQUEST)



post_create_view = PostView.as_view({'post': 'create'})
posts_list_view = PostView.as_view({'get': 'list'})
get_post_view = PostView.as_view({'get': 'retrieve'})

like_post_view = LikeView.as_view({'post': 'like'})
unlike_post_view = LikeView.as_view({'post': 'unlike'})


