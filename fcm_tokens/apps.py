from django.apps import AppConfig


class FcmTokensConfig(AppConfig):
    name = 'fcm_tokens'
