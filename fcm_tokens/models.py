from django.db import models

from user.models import Profile
from utils.models import BaseModel


class FcmToken(BaseModel):
    profile = models.ForeignKey(Profile, related_name='fcm_tokens', on_delete=models.PROTECT)
    token_value = models.TextField()

    class Meta:
        ordering = ('-created', 'id')


