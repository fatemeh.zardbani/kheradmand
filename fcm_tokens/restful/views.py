from rest_framework import status, viewsets
from rest_framework.response import Response

from fcm_tokens.models import FcmToken
from fcm_tokens.restful.serializers import FcmTokenSerializer


class FcmTokenView(viewsets.ModelViewSet):
    serializer_class = FcmTokenSerializer

    def perform_create(self, serializer):
        if not check_token_exist(self.request.data['token_value'], self.request.user.profile):
            serializer.save(profile=self.request.user.profile)
        else:
            return Response(status=status.HTTP_200_OK)

    def get_queryset(self):
        return FcmToken.objects.all()

    # def get_object(self):
    #     return self.get_queryset().get(id=self.kwargs.get('fcm_token_id'))


fcm_token_create_view = FcmTokenView.as_view({'post': 'create'})


def check_token_exist(token, profile):
    token_list=FcmToken.objects.filter(token_value=token).filter(profile=profile)
    if len(token_list)>0:
        return True
    return False