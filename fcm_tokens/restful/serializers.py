from rest_framework import serializers

from fcm_tokens.models import FcmToken
from user.restful.serializers import ProfileSerializer


class FcmTokenSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(read_only=True, many=False)
    token_value =serializers.CharField()
    class Meta:
        model = FcmToken
        fields = ('id', 'profile', 'token_value')
        read_only_fields = ('id', 'profile')

    def create(self, validated_data):
        print('avalidData:'+str(validated_data))
        # request = self.context.get("request")
        # print(str(request.data))
        # check_token_exist(token, profile)
        return super().create(validated_data)

