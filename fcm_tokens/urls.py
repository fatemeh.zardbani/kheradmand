from django.urls import path

from fcm_tokens.restful.views import fcm_token_create_view

urlpatterns = [
    path('refresh_token/', fcm_token_create_view),
]