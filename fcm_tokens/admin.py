from django.contrib import admin
from fcm_tokens.models import FcmToken

admin.site.register(FcmToken)
